#!/bin/sh

set -e

. /etc/dbconfig-common/puppetdb.conf

if [ $dbc_dbtype = "none" ]; then
    # nothing to do here
    exit
fi

. /usr/share/dbconfig-common/internal/pgsql

if ! dbc_pgsql_db_installed; then
    dbc_logline "postgresql is missing, abort"
    exit
fi

# enable required pg_trgm extension

_dbc_pgsql_exec_command "CREATE EXTENSION IF NOT EXISTS pg_trgm;"

# create read-only user for PuppetDB
# the application will still work if it's missing, but the log will contain warnings

dbc_dbwriteuser=$dbc_dbuser
dbc_dbreaduser="${dbc_dbuser}_read"
dbc_dbuser=$dbc_dbreaduser

if dbc_pgsql_check_user; then
    dbc_logline "${dbc_dbreaduser} already exists, skipping"
    exit
elif grep -qF "[database-read]" /etc/puppetdb/conf.d/database.ini; then
    dbc_logline "[database-read] config section already exists, skipping"
    exit
fi

dbc_dbpass=$(env LANG=C LC_ALL=C tr -dc "[:alnum:]" < /dev/urandom | dd bs=1 count=12 2>/dev/null)

dbc_pgsql_createuser

cat << EOF >> /etc/puppetdb/conf.d/database.ini

[read-database]
subname = //${dbc_dbserver}/${dbc_dbname}
username = ${dbc_dbreaduser}
password = ${dbc_dbpass}
EOF

# adjust permissions

_dbc_pgsql_exec_command "REVOKE CREATE ON SCHEMA public FROM public;
GRANT CREATE ON SCHEMA public TO ${dbc_dbwriteuser};
GRANT SELECT ON ALL TABLES IN SCHEMA public TO ${dbc_dbreaduser};
ALTER DEFAULT PRIVILEGES FOR USER ${dbc_dbwriteuser} IN SCHEMA public GRANT SELECT ON TABLES TO ${dbc_dbreaduser};
GRANT ${dbc_dbreaduser} TO ${dbc_dbwriteuser};"
