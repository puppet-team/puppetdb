puppetdb (8.4.1-3) unstable; urgency=medium

  * d/patches: add patch to fix tests with pgsql 17 (Closes: #1084038)
    Thanks to Rob Browning for providing this patch.

 -- Jérôme Charaoui <jerome@riseup.net>  Sat, 23 Nov 2024 22:34:12 -0500

puppetdb (8.4.1-2) unstable; urgency=medium

  * remove obsolete logrotate config on upgrade (Closes: #941080)
  * d/control: add Rules-Requires-Root: no
  * d/control: bump Standards-Version, no changes needed
  * d/tests: add allow-stderr to restrictions (Closes: #1077976)

 -- Jérôme Charaoui <jerome@riseup.net>  Fri, 30 Aug 2024 12:32:52 -0400

puppetdb (8.4.1-1) unstable; urgency=medium

  * New upstream version 8.4.1 (Closes: #1062330, #1064677)
  * d/control:
    + add lower/upper bounds to puppet dependency
    + update build dependencies
    + add new test dependencies
    + mark test.chuck build-dep as test-only
    + terminus add-on Enhances: puppetserver
  * d/patches:
    + refresh patches for new upstream version
    + add patch to downgrade to jetty9
    + skip new failing testcase
    + add patch to resolve ftbfs with java 21 (Closes: #1057678)
    + mark nrepl removal as not-needed upstream
  * d/tests:
    + adjust dependencies
    + switch to structured facts
    - drop host-core module from autopkgtest deps

 -- Jérôme Charaoui <jerome@riseup.net>  Sun, 31 Mar 2024 15:00:20 -0400

puppetdb (7.12.1-3) unstable; urgency=medium

  * fix FTBFS from bogus versions in dependency poms
  * remove nrepl library requirement
  * d/tests: capture puppetserver logs in test artifacts
  * only restart on abormal failure conditions

 -- Jérôme Charaoui <jerome@riseup.net>  Wed, 08 Feb 2023 18:58:45 -0500

puppetdb (7.12.1-2) unstable; urgency=medium

  * fix piuparts error

 -- Jérôme Charaoui <jerome@riseup.net>  Mon, 30 Jan 2023 09:09:35 -0500

puppetdb (7.12.1-1) unstable; urgency=medium

  * New upstream version 7.12.1
  * systemd service unit changes
    + don't enable systemd service unit at installation
    + sync service manager with app internal state
    + add longer startup timeout for service unit
    + timeout ExecReload after 60 seconds
  * puppetdb script changes:
    + rework puppetdb script binary
    + add ssl-setup subcommand to puppetdb script
  * maintscript changes:
    + setup read-only puppetdb user on install or upgrade
    + ensure dbconfig read-only database user is removed
  * d/control:
    + update package descriptions
    + fix Build-Deps incorrectly flagged as test-only
    + update puppet-terminus-puppetdb depends
    + bump Standards-Version, no changes needed
    - lsb-base is no longer needed
  * d/tests
    + re-enable puppetserver integration test
    + start puppetdb in standalone test
    + test puppetdb binary and reloading service
  * d/patches:
    + exclude docs and config examples from jar
    + fix log file paths
    + puppetdb may already be running on testbed
    + add test for resource export/collection
  * update README.Debian

 -- Jérôme Charaoui <jerome@riseup.net>  Sun, 29 Jan 2023 10:56:42 -0500

puppetdb (7.12.0-1) unstable; urgency=medium

  * New upstream version 7.12.0
  * d/control: switch back binary dep to default JRE (Closes: #1026163)
  * d/control: bump honeysql dependency to v2
  * d/patches: refresh patches for new upstream version

 -- Jérôme Charaoui <jerome@riseup.net>  Thu, 12 Jan 2023 10:57:15 -0500

puppetdb (7.11.2-3) unstable; urgency=medium

  * d/control: mark puppetdb-doc as M-A: foreign
  * Security issues fixed in 7.x series: CVE-2020-7943, CVE-2021-27019,
    CVE-2021-27021, CVE-2021-27023

 -- Jérôme Charaoui <jerome@riseup.net>  Sun, 27 Nov 2022 14:18:55 -0500

puppetdb (7.11.2-2) unstable; urgency=medium

  * Upload to unstable
  * d/patches: add patch to avoid lein error due to java 17
  * d/patches: add patch for postgresql-15 compatibility
  * d/source/lintian-override: fix lintian false positive

 -- Jérôme Charaoui <jerome@riseup.net>  Fri, 25 Nov 2022 08:43:43 -0500

puppetdb (7.11.2-1) experimental; urgency=medium

  * New upstream version 7.11.2
  * d/patches: rebase patches
  * d/rules: determine pgsql version automatically
  * run testsuite with pgsql-14 temporarily, no pgsql-15 support yet

 -- Jérôme Charaoui <jerome@riseup.net>  Fri, 21 Oct 2022 09:42:42 -0400

puppetdb (7.10.1-1) experimental; urgency=medium

  * New upstream version 7.10.1 (Closes: #990419, #1012577)
  * Switch d/watch to git mode
  * Switch to leiningen build system
  * Fix upstream build tests
  * d/patches:
    - Refresh patches
    - Add patch to replace not-packaged yaml.core in tests
    - new patch to adjust paths in config.ini
    - Reorder patch, put Lein Local first
    - Prevent failure of disable-update-checking test
  * d/control:
    - update Build-Deps
    - bump minimum versions for 2 dependencies
    - add myself to Uploaders
    - tag test-specific Build-Deps
    - bump debhelper-compat to 13
    - fix dependency loop caused by #929685
  * d/rules:
    - fix installation of jar file
    - fix installation of database.ini
  * d/tests:
    - avoid test failure when logfile is missing
    - abort if puppetdb.service enters failed state
    - add Restrictions: isolation-container
    - disable puppet integration test for now
  * d/copyright:
    - add separate Apache-2.0 license stanza
    - add myself to debian/* copyright
    - add paragraph for new js source code
    - add missing license texts
  * Run wrap-and-sort -bastk
  * Bump Standards-Version to 4.6.1, no changes needed
  * Remove StandardOutput=syslog from service unit
  * Add d/upstream/metadata

 -- Jérôme Charaoui <jerome@riseup.net>  Fri, 15 Jul 2022 17:57:36 -0400

puppetdb (6.2.0-4) unstable; urgency=medium

  * Make PDB schema bootstrappable with PostgreSQL 11.4 (Closes: #932135)
  * Bump Standards-Version to 4.4.0; no changes needed
  * Bump dh compat to 12
    + Pre-Depend on ${misc:Pre-Depends} to allow dh to inject pre-dependency
      on init-system-helpers

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Tue, 16 Jul 2019 21:02:45 -0300

puppetdb (6.2.0-3) unstable; urgency=medium

  * Drop B-D on activemq; support removed upstream

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Wed, 20 Feb 2019 08:42:16 +0200

puppetdb (6.2.0-2) unstable; urgency=medium

  * autopkgtests: fix race condition in puppet test
  * autopkgtests: check status in a more robust manner

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Fri, 08 Feb 2019 22:56:57 +0200

puppetdb (6.2.0-1) unstable; urgency=medium

  * New upstream release
  * Drop patches merged upstream:
     - 0001-Do-not-use-pooled-ActiveMQ-connections.patch
     - 0001-PDB-3993-Don-t-lookup-namespaces-to-compute-cli-subc.patch
     - honeysql-0.7-compat.patch
  * systemd: use the new puppetdb service entrypoint
  * Add puppet integration DEP-8 test
  * database.ini: drop obsolete log-slow-statements option

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Fri, 08 Feb 2019 12:43:03 +0200

puppetdb (4.4.1-3) unstable; urgency=medium

  * Fix startup issues with JRE 11/Clojure 1.9 (Closes: #878193)
    + Backport upstream change to not lookup namespaces to compute cli
      subcommands; modify to not expose the (non-working in Debian) benchmark
      commands
    * (B-)Depend on libgeronimo-j2ee-management-1.1-spec-java and add the jar
      to the classpath
  * Bump Standards-Version to 4.3.0; no changes needed
  * d/copyright: bump debian/ years
  * Bump dh compat to 11; no changes needed
  * Drop the logrotate config, rotation is handled by logback (Closes:
    #881584)

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Fri, 08 Feb 2019 01:02:24 +0200

puppetdb (4.4.1-2) unstable; urgency=medium

  * puppetdb.service: expand $JAVA_ARGS
  * Drop B-D on glassfish-javaee (Closes: #884186)
  * Drop B-D on dh-systemd
  * Switch Vcs-* URLs to salsa.d.o
  * Ship NOTICE.txt as required by the Apache License, Version 2.0

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Wed, 10 Oct 2018 17:45:23 +0300

puppetdb (4.4.1-1) unstable; urgency=medium

  * Initial release (Closes: #673515)

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Sat, 05 Aug 2017 18:44:07 -0400
